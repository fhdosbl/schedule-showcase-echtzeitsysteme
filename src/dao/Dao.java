package dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import core.tools.Logger;

public class Dao {

	private static Logger LOGGER = Logger.getLogger(Dao.class.getSimpleName());

	public static void saveProcessList(List<core.Process> processes, File file) {
		String datanm = "cache.ser";
		ObjectOutputStream aus = null;
		try {
			aus = new ObjectOutputStream(new FileOutputStream(file));
			aus.writeObject(processes);
		} catch (IOException ex) {
			System.out.println(ex);
		} finally {
			try {
				if (aus != null) {
					aus.flush();
					aus.close();
				}
			} catch (IOException e) {
			}
		}
		LOGGER.info("ProcessList saved");

	}

	public static List<core.Process> readProcessList(String path) {
		String datanm = "cache.ser";
		List<core.Process> cache = null;
		ObjectInputStream objectInputStream = null;
		FileInputStream inputFileStream = null;
		try {
			inputFileStream = new FileInputStream(path);
			objectInputStream = new ObjectInputStream(inputFileStream);
			cache = (List<core.Process>) objectInputStream.readObject();

		} catch (FileNotFoundException ex) {
			LOGGER.info("Can not found data!");
			return null;
		} catch (Exception ex) {
			System.out.println(ex);
			return null;
		} finally {
			if (objectInputStream != null) {
				try {
					objectInputStream.close();
				} catch (IOException e) {
					// TODO Automatisch generierter Erfassungsblock
					e.printStackTrace();
				}
			} else if (inputFileStream != null) {
				try {
					inputFileStream.close();
				} catch (IOException e) {
					// TODO Automatisch generierter Erfassungsblock
					e.printStackTrace();
				}
			}
		}
		LOGGER.info("Read ProcessList");
		return cache;

	}

}
