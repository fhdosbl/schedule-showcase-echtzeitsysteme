package frontend;

import core.Process;
import core.tools.Logger;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class ProcessView extends JPanel {

	private static Logger LOGGER = Logger.getLogger(ProcessView.class.getSimpleName());

	private JLabel lid, id, lexec, lperi, larriv, ldead, lprior, lrel;
	private JTextField exec, peri, arriv, deadl, prior, rel;
	private Process process;

	private Font menuFont = new Font(Font.MONOSPACED, Font.PLAIN, 12);
	private Border menuBorder = BorderFactory.createEtchedBorder();
	private Color backgroundColor = new Color(250, 250, 250);

	public ProcessView() {
		this.setLayout(new GridLayout(5, 2));
		this.setBorder(menuBorder);
		process = new Process();
		initialize();
		this.setSize(new Dimension(200, 300));
		this.setVisible(true);
	}

	public ProcessView(Process p) {
		this.setLayout(new GridLayout(5, 2));
		this.setBorder(menuBorder);
		process = p;
		initialize();
		this.setSize(new Dimension(200, 300));
		this.setVisible(true);
	}

	private void initialize() {
		this.setBackground(backgroundColor);
		lid = new JLabel("ID:");
		id = new JLabel(String.valueOf(String.valueOf(process.getId())));

		lexec = new JLabel("Execution time:");
		exec = new JTextField(String.valueOf(process.getExecutionTime()));

		exec.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					process.setExecutiontime(Double.parseDouble(exec.getText()));
					LOGGER.debug("Set executiontime : " + process.getExecutionTime());
				} catch (Exception e1) {
					exec.setText("Only Numbers!");
				}
			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});

		larriv = new JLabel("Arrival time:");
		arriv = new JTextField(String.valueOf(process.getArrivalTime()));

		arriv.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					process.setArrivalTime(Double.parseDouble(arriv.getText()));
					LOGGER.debug("Set arrivaltime : " + process.getArrivalTime());
				} catch (Exception e2) {
					arriv.setText("Only Numbers!");
				}
			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});

		ldead = new JLabel("Deadline:");
		deadl = new JTextField(String.valueOf(process.getDeadline()));

		deadl.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					process.setDeadline(Double.parseDouble(deadl.getText()));

				} catch (Exception e2) {
					deadl.setText("Only Numbers!");
				}
				LOGGER.debug("Set deadline : " + process.getDeadline());

			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});

		lperi = new JLabel("Periode:");
		peri = new JTextField(String.valueOf(process.getPeriod()));

		peri.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				try {
					process.setPeriod(Double.parseDouble(peri.getText()));

				} catch (Exception e2) {
					peri.setText("Only Numbers!");
				}
				LOGGER.debug("Set periode : " + process.getPeriod());

			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});

		lprior = new JLabel("Priority:");
		prior = new JTextField(String.valueOf(process.getPriority()));

		prior.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					process.setPriority(Double.parseDouble(prior.getText()));
					LOGGER.debug("Set priority : " + process.getPriority());
				} catch (Exception e2) {
					prior.setText("Only Numbers!");
				}
			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});

		lrel = new JLabel("Release:");
		rel = new JTextField(String.valueOf(process.getReleaseTime()));

		rel.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					process.setReleaseTime(Double.parseDouble(rel.getText()));
					LOGGER.debug("Set releasetime : " + process.getReleaseTime());
				} catch (Exception e2) {
					rel.setText("Only Numbers!");
				}

			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});

		// Fonts

		larriv.setFont(menuFont);
		ldead.setFont(menuFont);
		lexec.setFont(menuFont);
		lid.setFont(menuFont);
		lperi.setFont(menuFont);
		lprior.setFont(menuFont);
		lrel.setFont(menuFont);

		this.add(lid);
		this.add(id);

		// this.add(lprior);
		// this.add(prior);

		this.add(lexec);
		this.add(exec);

		this.add(lperi);
		this.add(peri);

		this.add(larriv);
		this.add(arriv);

		this.add(ldead);
		this.add(deadl);

		// this.add(lrel);
		// this.add(rel);

	}

	public Process getProcess() {
		return process;
	}

	public void updateFields(String text) {
		switch (text) {
		case "EDD":
			arriv.setEnabled(false);
			deadl.setEnabled(true);
			rel.setEnabled(false);
			prior.setEnabled(false);
			exec.setEnabled(true);
			peri.setEnabled(false);
			break;

		case "EDF":
			arriv.setEnabled(true);
			deadl.setEnabled(true);
			rel.setEnabled(false);
			prior.setEnabled(false);
			exec.setEnabled(true);
			peri.setEnabled(false);
			break;

		case "LLF":
			arriv.setEnabled(true);
			deadl.setEnabled(true);
			rel.setEnabled(false);
			prior.setEnabled(false);
			exec.setEnabled(true);
			peri.setEnabled(true);
			break;

		default:
			arriv.setEnabled(true);
			deadl.setEnabled(true);
			rel.setEnabled(true);
			prior.setEnabled(true);
			exec.setEnabled(true);
			peri.setEnabled(true);
		}

	}

	/***
	 * We dont need Release and Priority for this schedules! So we set them to
	 * 0! This is neccassary, because in EDF and others, the release will be
	 * used and have to be init with 0!
	 * 
	 * Furthermore we update all inputs (fix a bug)
	 */
	public void updateInput() {
		process.setArrivalTime(Double.parseDouble(arriv.getText()));
		process.setDeadline(Double.parseDouble(deadl.getText()));
		process.setExecutiontime(Double.parseDouble(exec.getText()));
		process.setPeriod(Double.parseDouble(peri.getText()));
		process.setReleaseTime(0);
		process.setPriority(0);
	}

}
