package core.tools;

import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.event.EventListenerList;

public class Logger {
	public static Logger logger = null;
	private static ArrayList<LogginInfo> log;
	private String name;
	private static EventListenerList eventListener = new EventListenerList();

	public void addListener(LogListener listener) {
		eventListener.add(LogListener.class, listener);
	}

	public void removeListener(LogListener listener) {
		eventListener.remove(LogListener.class, listener);
	}

	protected void fire(LEVEL event) {
		Object[] listeners = eventListener.getListenerList();
		// loop through each listener and pass on the event if needed
		int numListeners = listeners.length;
		for (int i = 0; i < numListeners; i += 2) {
			if (listeners[i] == LogListener.class) {
				// pass the event to the listeners event dispatch method
				((LogListener) listeners[i + 1]).gotLog(event);
			}
		}
	}

	private Logger() {
		log = new ArrayList<>();
	}

	private Logger(String name) {
		this.name = name;
	}

	public static Logger getLogger(String classname) {
		if (logger == null) {
			logger = new Logger();
		}

		return new Logger(classname);
	}

	public void info(String message) {
		Calendar cal = Calendar.getInstance();
		String logging = "[" + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":"
				+ cal.get(Calendar.SECOND) + "] " + LEVEL.INFO.toString() + " (" + name + "):  " + message;
		log.add(new LogginInfo(logging, LEVEL.INFO));
		fire(Logger.LEVEL.INFO);
	}

	public void error(String message) {
		Calendar cal = Calendar.getInstance();
		String logging = "[" + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":"
				+ cal.get(Calendar.SECOND) + "] " + LEVEL.ERROR.toString() + " (" + name + "):  " + message;
		log.add(new LogginInfo(logging, LEVEL.ERROR));
		fire(Logger.LEVEL.ERROR);
	}

	public void debug(String message) {
		Calendar cal = Calendar.getInstance();
		String logging = "[" + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":"
				+ cal.get(Calendar.SECOND) + "] " + LEVEL.DEBUG.toString() + " (" + name + "):  " + message;
		log.add(new LogginInfo(logging, LEVEL.DEBUG));
		fire(Logger.LEVEL.DEBUG);
	}

	public ArrayList<LogginInfo> getLoggerMessages() {
		return log;
	}

	public enum LEVEL {
		ERROR, INFO, DEBUG;
	}

	public void clearLog() {
		log = new ArrayList<>();
	}

	public class LogginInfo {
		private final String log;
		private final LEVEL level;

		public LogginInfo(String log, LEVEL level) {
			this.log = log;
			this.level = level;
		}

		public String getLog() {
			return log;
		}

		public LEVEL getLevel() {
			return level;
		}
	}
}
