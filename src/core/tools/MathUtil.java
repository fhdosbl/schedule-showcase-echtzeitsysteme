package core.tools;

import java.util.List;

import core.Process;

public final class MathUtil {

	private MathUtil() {
		throw new RuntimeException("This class is not instantiable");
	}

	/*
	 * Thanks to
	 * https://stackoverflow.com/questions/4201860/how-to-find-gcd-lcm-on-a-set-
	 * of-numbers.
	 */
	public static long gcd(long a, long b) {
		while (b > 0) {
			long tmp = b;
			b = a % b;
			a = tmp;
		}
		return a;
	}

	public static long gcd(long... values) {
		long result = values[0];
		for (int i = 1; i < values.length; i++)
			result = gcd(result, values[i]);
		return result;
	}

	public static long lcm(long a, long b) {
		return a * (b / gcd(a, b));
	}

	public static long lcm(long... values) {
		long result = values[0];
		for (int i = 1; i < values.length; i++)
			result = lcm(result, values[i]);
		return result;
	}

	public static float calculateUtilization(List<Process> processes) {
		float sum = 0;
		for (Process p : processes)
			sum += p.getExecutionTime() / p.getPeriod();
		return sum;
	}
}
