package core.tools;

import java.util.EventListener;

public interface LogListener extends EventListener{
	void gotLog(Logger.LEVEL e);
}
