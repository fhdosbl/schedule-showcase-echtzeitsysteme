package core.tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import core.Process;
import de.nixosoft.jlr.JLRConverter;
import de.nixosoft.jlr.JLRGenerator;
import de.nixosoft.jlr.JLROpener;

public class LatexGenerator {

	List<Process> processOrder;
	String projectName;
	int taskNum;
	int majorCycle;

	ArrayList<ArrayList<String>> taskList = new ArrayList<ArrayList<String>>();

	Logger LOGGER = Logger.getLogger(LatexGenerator.class.getSimpleName());

	public LatexGenerator(List<Process> processOrder, String projectName, int taskNum, int majorCycle) {
		this.processOrder = processOrder;
		this.projectName = projectName;
		this.taskNum = taskNum;
		this.majorCycle = majorCycle;
		LOGGER.info(" initialized " + majorCycle + " " + taskNum);
	}

	public File createPdf() {

		File paths[] = createPdfPath();

		LOGGER.info(" Paths generated");

		File template = paths[0];
		File scheduling = paths[1];
		File workingDirectory = paths[2];
		File outDir = paths[3];

		LOGGER.info(" Adding processes to Latex list");
		/* Adding processes (data) to List */
		for (Process process : processOrder) {
			LOGGER.debug("ADD Process (" + process.getId() + "): \nID-" + process.getId() + " \nAR-Time: "
					+ process.getArrivalTime() + " \nRel-Time: " + process.getReleaseTime() + "\nDeadline: "
					+ process.getDeadline() + "\nPeriode: " + process.getPeriod() + "\nPrio: " + process.getPriority()
					+ "\nExec-Time: " + process.getExecutionTime());
			addProcess(process);

			LOGGER.info(" Added process " + process.getId());
		}

		File returnFile = create(template, scheduling, workingDirectory, outDir, false);

		return returnFile;
	}

	public void showPdf(String path) throws IOException {
		File f = new File(path);
		LOGGER.info(" Opening PDF...");
		JLROpener.open(f);
	}

	private File create(File template, File scheduling, File workingDirectory, File outDir, boolean openPDF) {
		JLRConverter converter = new JLRConverter(workingDirectory);

		LOGGER.info(" Start PDF Creation");

		converter.replace("projectName", projectName + "");
		converter.replace("taskNum", taskNum + "");
		converter.replace("majorCycle", majorCycle + "");

		converter.replace("taskList", taskList);

		try {
			if (!converter.parse(template, scheduling)) {

				LOGGER.error(" " + converter.getErrorMessage());

				System.out.println(converter.getErrorMessage());
			}

			JLRGenerator pdfGen = new JLRGenerator();

			if (!pdfGen.generate(scheduling, outDir, workingDirectory)) {
				LOGGER.error(" " + pdfGen.getErrorMessage());
			}

			if (openPDF) {
				LOGGER.info(" Opening PDF...");
				JLROpener.open(pdfGen.getPDF());
			}

			return pdfGen.getPDF();

		} catch (IOException e) {
			LOGGER.info(" PDF could not be created..."
					+ "\nPlease ensure MikTeX is installed to create PDF files from LaTeX!\n(Also install the package: tikz-timing with the MikTeX-Packagemanager)");
		}
		return null;
	}

	private File[] createPdfPath() {
		File f = new File("");
		File workingDirectory = new File(f.getAbsolutePath() + File.separator + "src"); // get
																						// current
																						// directory

		File template = new File(workingDirectory.getAbsolutePath() + File.separator + "schedulingTemplate.tex");

		File currentDir = new File("");
		File outDir = new File(currentDir.getAbsolutePath() + File.separator + "out");
		if (!outDir.isDirectory()) {
			LOGGER.info(" Create out folder...");
			outDir.mkdir();
		}

		File scheduling = new File(outDir.getAbsolutePath() + File.separator + "scheduling.tex");

		File paths[] = new File[4];
		paths[0] = template;
		paths[1] = scheduling;
		paths[2] = workingDirectory;
		paths[3] = outDir;

		return paths;
	}

	public void addProcess(Process proc) {
		ArrayList<String> taskData = new ArrayList<String>();
		taskData.add(0, proc.getId() + "");
		taskData.add(1, proc.getArrivalTime() + ""); /* expected arrival time */
		taskData.add(2, proc.getDeadline() + "");
		taskData.add(3, proc.getReleaseTime() + ""); /* real arrival time */
		taskData.add(4, (proc.getExecutionTime()+proc.getReleaseTime()) + "");
		taskList.add(taskData);
	}

}
