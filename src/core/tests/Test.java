package core.tests;

import java.util.List;

import core.Process;

public abstract class Test {

	private TestResult result;
	private Object resultValue;
	private List<Process> processList;

	public Test(List<Process> processList) {
		this.processList = processList;
	}

	public void setResult(TestResult res) {
		result = res;
	}

	public boolean hasResultValue() {
		return this.resultValue != null;
	}

	public TestResult getResult() {
		return result;
	}

	public Object getResultValue() throws IllegalStateException {
		if (this.resultValue == null)
			throw new IllegalStateException("Test has no result value!");
		return resultValue;
	}

	public List<Process> getProcessList() {
		return processList;
	}

	public abstract void start();

	public enum TestResult {
		FAILED, PASSED, POSSIBLE;
	}
}
