package core.tests;

import java.util.List;

import core.Process;
import core.tools.Logger;

public class HBTest extends Test {
	private static Logger LOGGER = Logger.getLogger(HBTest.class.getSimpleName());

	public HBTest(List<Process> processList) {
		super(processList);
		// TODO Auto-generated constructor stub
	}
	
	public void start(){
		LOGGER.info("Start HB-Test");
		double result =1;
		try{
			for(Process p : super.getProcessList()){
		
			result *= ((p.getExecutionTime()/p.getPeriod())+1);
			LOGGER.info("HB_Test calculation: result *= ("+p.getExecutionTime() +" / "+p.getPeriod()+") +1");
		}
			
		}catch(ArithmeticException e){
			LOGGER.error("Periode != 0 : "+e.getMessage());
			setResult(TestResult.FAILED);
			return;
		}
		if(result <= 2){
			LOGGER.info("HB_Test PASSED");
			setResult(TestResult.PASSED);
			return;
		}
		LOGGER.info("HB_Test FAILED");
		setResult(TestResult.FAILED);
	}
	

}
