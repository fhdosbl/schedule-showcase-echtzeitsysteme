package core.tests;

import java.util.List;

import core.Process;
import core.tools.Logger;

public class LLTest extends Test {
	private static Logger LOGGER = Logger.getLogger(LLTest.class.getSimpleName());

	public LLTest(List<Process> processList) {
		super(processList);
		// TODO Auto-generated constructor stub
	}
	
	public void start(){
		LOGGER.info("Start LL-Test");
		double u =0;
		int n = 0;
		double u_min =0;
		try{
			for(Process p : super.getProcessList()){
				n++;
				u += (p.getExecutionTime()/p.getPeriod());
				LOGGER.info("calculation u += "+p.getExecutionTime() +" / " + p.getPeriod());
			}
			double exp = (1.0/n);
			u_min = n*(Math.pow(2, exp)-1);
			LOGGER.info("calculation u_min = n*(2^"+exp+"-1)");
			LOGGER.info("calculation u = "+u+"  u_min = "+u_min+" n = "+n);
			if(u <= u_min){
				LOGGER.info("LLTest PASSED");
				setResult(TestResult.PASSED);
				return;
			}else if(u <= 1){
				LOGGER.info("LLTest FAILED but chance of possible solution (u<=1)");
				setResult(TestResult.POSSIBLE);
				return;
			}
			
		}catch(Exception e){
			LOGGER.error("LLTest error: "+e.getMessage());
			setResult(TestResult.FAILED);
			return;
		}
		
		LOGGER.info("LLTest FAILED");
		setResult(TestResult.FAILED);
	}
	

}
