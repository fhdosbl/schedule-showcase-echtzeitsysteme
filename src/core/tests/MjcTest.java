package core.tests;

import java.util.List;

import core.Process;
import core.tools.Logger;
import core.tools.MathUtil;

public class MjcTest extends Test {

	private final Logger logger = Logger.getLogger(getClass().getSimpleName());

	public MjcTest(List<Process> processList) {
		super(processList);
	}

	@Override
	public void start() {
		long[] periods = new long[getProcessList().size()];
		for (int i = 0; i < getProcessList().size(); i++)
			periods[i] = (long) getProcessList().get(i).getPeriod();
		long finishTime = MathUtil.lcm(periods);
		logger.info(String.format("Number of cycles: %d", finishTime));
		setResult(TestResult.PASSED);
	}

}
