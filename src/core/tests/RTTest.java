package core.tests;

import java.util.List;

import core.Process;
import core.tools.Logger;

public class RTTest extends Test {
	private static Logger LOGGER = Logger.getLogger(RTTest.class.getSimpleName());
	private int RT_ITERATION_LIMIT = 10;
	
	public RTTest(List<Process> processList) {
		super(processList);
		// TODO Auto-generated constructor stub
	}
	
	public void start(){
		LOGGER.info("Start RT Test");
		double result = 0;
		double result_old = 0;
		
		//Go above all process
		for(int j = super.getProcessList().size()-1; j < super.getProcessList().size();j++){
			/***
			 * Start algo for RT_ITERATION_LIMIT iterations
			 */
			for(int i = 0; i < RT_ITERATION_LIMIT;i++){
				if(i == 0){
					for(int k = i; k <super.getProcessList().size();k++ ){
						result += super.getProcessList().get(k).getExecutionTime();
						LOGGER.info("Result-STEP calculation :  reult += "+super.getProcessList().get(k).getExecutionTime());
					}		
				}else {
					result = super.getProcessList().get(j).getExecutionTime();
					for(int k = 0; k <super.getProcessList().size()-1;k++ ){
						result += Math.ceil((result_old/super.getProcessList().get(k).getPeriod()))*super.getProcessList().get(k).getExecutionTime();
						LOGGER.info("Result-STEP calculation :  result += |`"+result_old+" / "+super.getProcessList().get(k).getPeriod()+"�|  *  "+ super.getProcessList().get(k).getExecutionTime());
					}	
				}
				
				LOGGER.info("Result:   "+result);
				
				//If convergent result, test passed for this process
				if(result == result_old){
					if(result > super.getProcessList().get(j).getPeriod()){
						LOGGER.info("RT Test Failed caused on condition (result > periode)");
						setResult(TestResult.FAILED);
						return;
					}
					break;
				}
				//If not, test failed
				if(i == RT_ITERATION_LIMIT-1){
					LOGGER.info("RT Test Failed caused on condition (To many steps)");
					setResult(TestResult.FAILED);
					return;
				}
				result_old = result;
				result = 0;	
			}		
		}
		LOGGER.info("RT Test Passed");
		setResult(TestResult.PASSED);
		return;
	}
	

}
