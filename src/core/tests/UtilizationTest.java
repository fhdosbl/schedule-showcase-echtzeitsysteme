package core.tests;

import java.util.List;

import core.Process;
import core.tools.Logger;
import core.tools.MathUtil;

public class UtilizationTest extends Test {

	private final Logger logger = Logger.getLogger(getClass().getSimpleName());

	public UtilizationTest(List<Process> processList) {
		super(processList);
	}

	@Override
	public void start() {
		final float utilization = MathUtil.calculateUtilization(getProcessList());
		if (utilization > 1.0) {
			logger.info(String.format("Utilization > 1: %.2f", utilization));
			setResult(TestResult.FAILED);
		} else {
			logger.info(String.format("Utilization: %.2f", utilization));
			setResult(TestResult.PASSED);
		}

	}

}
