package core;

import java.io.File;
import java.util.List;

import dao.Dao;

public class Management {
	public static int ID = 1; /**<-- Process ID **/
	private static Management management;
	private Dao dao;
	
	private Management(){
		
		
	}
	
	/***
	 * Singelton
	 * @return
	 */
	public static Management getInstance(){
		if(management == null){
			return new Management();
		}
		return management;
	}
	
	
	/***
	 * Save to file 
	 * @param processes
	 * @param file
	 */
	public void saveProcessPage(List<Process> processes, File file){
		dao.saveProcessList(processes,file);
	}
	
	/***
	 * Read from file
	 * @param path
	 * @return
	 */
	public List<Process> readProcess(String path){
		return dao.readProcessList(path);
	}

}
