package core;

import java.io.Serializable;

public class Process implements Serializable {

	private static final long serialVersionUID = -3669454229697845577L;

	private int id;
	private double executionTime;
	private double period;
	private double arrivalTime;
	private double deadline;
	private double priority;
	private double releaseTime;

	public Process() {
		id = Management.ID++;
	}

	public Process(Process p) {
		this.id = p.id;
		this.executionTime = p.executionTime;
		this.period = p.period;
		this.arrivalTime = p.arrivalTime;
		this.deadline = p.deadline;
		this.priority = p.priority;
		this.releaseTime = p.releaseTime;
	}

	public double getExecutionTime() {
		return executionTime;
	}

	public void setExecutiontime(double executiontime) {
		this.executionTime = executiontime;
	}

	public double getPeriod() {
		return period;
	}

	public void setPeriod(double periode) {
		this.period = periode;
	}

	public double getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(double arrivaltime) {
		this.arrivalTime = arrivaltime;
	}

	public double getDeadline() {
		return deadline;
	}

	public void setDeadline(double deadline) {
		this.deadline = deadline;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPriority() {
		return priority;
	}

	public void setPriority(double priority) {
		this.priority = priority;
	}

	public double getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(double double1) {
		this.releaseTime = double1;
	}

}
