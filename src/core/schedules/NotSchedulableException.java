package core.schedules;

public class NotSchedulableException extends Exception {

	private static final long serialVersionUID = 1L;

	public NotSchedulableException(String reason) {
		super(reason);
	}
}
