package core.schedules;

public class Property<T> {

	private final String name;
	private final T value;

	protected Property(String name, T value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public T getValue() {
		return value;
	}
}
