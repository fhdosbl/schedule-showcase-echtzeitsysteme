package core.schedules;

import java.util.ArrayList;
import java.util.List;

import core.Process;
import core.tests.Test;

public class Schedule {

	private int majorCycleTime;
	private List<Process> processOrder;
	private final List<Test> tests;
	private final List<Property<?>> properties;

	protected Schedule() {
		this.processOrder = new ArrayList<>();
		this.tests = new ArrayList<>();
		this.properties = new ArrayList<>();
	}

	public int getMajorCycleTime() {
		return majorCycleTime;
	}

	public void setMajorCycleTime(int majorCycleTime) {
		this.majorCycleTime = majorCycleTime;
	}

	public List<Test> getTestResults() {
		return tests;
	}

	public List<Property<?>> getProperty() {
		return properties;
	}

	public List<Process> getProcessOrder() {
		return processOrder;
	}

	public void setProcessOrder(List<Process> processOrder) {
		this.processOrder = processOrder;
	}

}
