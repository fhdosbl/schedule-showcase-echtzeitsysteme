package core.schedules;

import java.util.List;

import core.Process;

public interface Scheduler {
	public Schedule schedule(List<Process> processes) throws NotSchedulableException;
}
